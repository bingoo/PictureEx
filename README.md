# PictureEx
PictureEx图片显示类支持以下格式的图片：GIF (including animated GIF87a and GIF89a), JPEG, BMP, WMF, ICO, CUR等，我特别推崇的是可以做出动画，而且轻而易举，确实很COOL。

## 下面是详细的编程过程： 
1. 新建项目：在VC6中用MFC新建一个基于对话框的GifDemo应用程序，接受所有缺省选项即可；

2. 在项目中插入文件：把PictureEx.h，PictureEx.cpp文件copy 到项目文件夹下，Project-＞Add to Project-＞Files中选上PictureEx.h，PictureEx.cpp， Insert;

3. 加入图片控件：从对话框控件中把Picture Control（图片控件)拖入主对话框中，修改其属性：ID：IDC_GIF，TYPE：Rectangle，其余接受缺省选项。再在ClassWiard中为IDF_GIF加入CSatic控制变量m_GifPic, 注意看一下，GifDemoDlg.h中是否加上了#include "PictureEx.h "（由ClassWiard加入）。然后将CSatic m_GifPic;更改成CPictureEx m_GifPic; 

4. 加载动画文件：先将要加载的动画文件放到 res 资源文件夹下，再将其Import进项目中，由于MFC只支持256BMP文件的图片，因此，我们要新建一个图片类型： "GIF ",我在这里将我网站的宣传图片roaring.gif放进去 ，并将其ID修改成：IDR_GIFROARING。
> import(导入）gif动画的详细过程： 
> 在resourceview窗口中，单击鼠标右键，在出现的环境菜单中选择“import...”命令，会出现“import resource”选择文件对话框，文件类型选择“所有文件（*.*）”，open as 选项为 "auto ",再选择动画文件所在目录，选上要载入的动画文件 roaring.gif,再单击 import,由于gif动画类型不是vc默认的文件类型，这时会出现 "custom resource type "对话框，键入“ "gif "”，再单击ok，然后再修改其id。 

5. 在程序的适当位置添入加载代码： 这里，我们在CGifDemoDlg::OnInitDialog()函数中加入如下代码:
```C++
// TODO: Add extra initialization here 
if (m_GifPic.Load(MAKEINTRESOURCE(IDR_GIFROARING),_T( "Gif "))) 
    m_GifPic.Draw();

if (m_GifPic.Load(strFile))
    m_GifPic.Draw(); 

```
如果仅仅把动画载入，到这就可以了，运行一下，应该看看您的的成果了。

> 下面附带说说如何将这幅动画制作成超链接，以后，咱们也可以宣传自已的公司、网站或产品了。

6. 利用ClassWiard加入一个LButtonDown鼠标左键消息处理函数CGifDemoDlg::OnLButtonDown(UINT nFlags, CPoint point)， 添入如下代码：

```C++
void CGifDemoDlg::OnLButtonDown(UINT nFlags, CPoint point) 
{
    // TODO: Add your message handler code here and/or call default
    CRect rect;
    m_GifPic.GetWindowRect(&rect);
    ScreenToClient(&rect);
    if (rect.PtInRect(point))
    ShellExecute(AfxGetMainWnd()->m_hWnd,_T("open"),
    _T("http://roaringwind.best.163.com"),_T(""),NULL,0);
    CDialog::OnLButtonDown(nFlags, point);
}
```

我在这儿将我主页的地址放上了，运行，点击动画图片就能进入我的站点的了。当然要是能象所有的超链接一样，能将鼠标变成手形，就更好了。

7. 改变鼠标形状：将一个鼠标文件放在res文件夹中，IMPORT，ID：IDC_CURSOR1，利用ClassWiard加入一个WM_SETCURSOR消息处理函数CGifDemoDlg::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)， 添入如下代码：
```C++
BOOL CGifDemoDlg::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
    // TODO: Add your message handler code here and/or call default
    CRect rect;
    m_GifPic.GetWindowRect(&rect);
    ScreenToClient(&rect);
    CPoint point;
    GetCursorPos(&point);
    ScreenToClient(&point);
    if (rect.PtInRect(point) && m_hCursor)
    {
        SetCursor(m_hCursor);
        return TRUE;
    };
    return CDialog::OnSetCursor(pWnd, nHitTest, message);
}
```